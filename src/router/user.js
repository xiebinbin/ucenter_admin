import UserList from '@/components/user/List'
import UserEdit from '@/components/user/Edit'
export default [
  {
    path: '/user/list',
    name: 'UserList',
    component: UserList
  },
  {
    path: '/user/edit',
    name: 'UserEdit',
    component: UserEdit
  }
]
